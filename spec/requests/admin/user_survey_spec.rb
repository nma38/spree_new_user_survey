require 'spec_helper'

describe "UserSurvey" do
  context "as_admin_user" do
    before do
      user = create(:admin_user, :email => "test@example.com")
      sign_in_as!(user)
      visit spree.admin_path
    end
   
    it "should have link to user_surveys" do
      page.should have_content("User Surveys")
    end
    
    it "should be able to create a new user survey" do
      click_link "User Surveys"
      click_link "New User Survey"
      page.should have_content("Creating User Survey")
      
      fill_in 'Title', :with => 'hello test'
      click_button 'create'
      
      click_link "User Surveys"
      within('table tbody') { page.should have_content("hello test")}
    end

    context "editing a user survey" do
      before(:each) do
        @user_survey = create(:user_survey)
        click_link "User Surveys"
        within('table tbody tr:nth-child(1)') { click_link "Edit" }
      end

      it "should be able to update title" do
        old_title = @user_survey.title
        new_title = 'my new title'
        fill_in 'Title', :with => :new_title

        click_button "update"

        click_link "User Surveys"

        within('table tbody') { page.should_not have_content(:old_title)}
        within('table tbody') { page.should have_content(:new_title)}

      end
    end
  end
end
