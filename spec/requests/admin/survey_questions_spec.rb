require 'spec_helper'
describe "UserSurveyQuestion" do
  context "as_admin_user" do
    before do
      user = create(:admin_user, :email => "test@example.com")
      sign_in_as!(user)
    end

    before do
      visit spree.admin_path
    end

    context "accessing user surveys" do
      context "no user surveys" do
        it "should not show any questions" do 
          click_link "User Surveys"
          # check to make sure not questions pop up
        end
      end

      context "some user survey" do
        before(:each) do
          @user_survey = create(:user_survey)
          click_link "User Surveys"
          within('table.index tbody tr:nth-child(1)') { click_link "Edit" }
        end

        it "should have a questions tab" do
          page.should have_content("Questions")
        end

        it "should let me view survey questions" do
          survey_question = create(:user_survey_question, :user_survey => @user_survey)
          other_survey_question = create(:user_survey_question)
          click "Questions"
          page.should have_content(survey_question.question)
          page.should_not have_content(other_survey_question.question)
        end
      end
    end
  end
end
