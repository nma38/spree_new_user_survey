require 'spec_helper'

describe Spree::UserSurveyAnswer do
  it "should have a foreign key to a user_survey_question" do
    survey_answer = FactoryGirl.build(:user_survey_answer)
    survey_answer.should respond_to(:user_survey_question)
  end

  it "should have a set of answer options", :broken => true do
    survey_answer = FactoryGirl.build(:user_survey_answer)
    survey_answer.should respond_to(:user_survey_answer_option)
  end

  it "should have a foreign key to a user" do
    survey_answer = FactoryGirl.build(:user_survey_answer)
    survey_answer.should respond_to(:user)
  end
end
