require 'spec_helper'

describe Spree::UserSurveyQuestion do
  # presume all question are multiple choice types
  
  it "should have a question" do
    survey_question = FactoryGirl.build(:user_survey_question)
    survey_question.should respond_to(:question)
  end

  it "should belong to a user_survey" do
    survey_question = FactoryGirl.build(:user_survey_question)
    survey_question.should respond_to(:user_survey)
  end

  it "should have a set of options" do
    survey_question = FactoryGirl.build(:user_survey_question)
    survey_question.should respond_to(:question_options)
  end

  it "should have a question type" do
    survey_question = FactoryGirl.build(:user_survey_question)
    survey_question.should respond_to(:question_type)
  end
end
