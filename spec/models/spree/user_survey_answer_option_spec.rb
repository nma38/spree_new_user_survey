require 'spec_helper'

describe Spree::UserSurveyAnswerOption do
  
  it "should belong to a survey answer" do
    survey_answer_option = Factory.build(:user_survey_answer_option)
    survey_answer_option.should respond_to(:user_survey_answer)
  end

  it "should have option name" do
    survey_answer_option = Factory.build(:user_survey_answer_option)
    survey_answer_option.should respond_to(:option_id)
  end

end
