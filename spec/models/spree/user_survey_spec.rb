require 'spec_helper'

describe Spree::UserSurvey do
  it "should have user survey questions" do
    user_survey = FactoryGirl.build(:user_survey)
    user_survey.should respond_to(:user_survey_question)
  end

  it "should have users" do
    user_survey = FactoryGirl.build(:user_survey)
    user_survey.should respond_to(:user)
  end
  
  it "should have a survey title" do
    user_survey = FactoryGirl.build(:user_survey)
    user_survey.should respond_to(:title)
  end

  context "# new user" do
    #let(:user) { Factory.create(:user) }
    #let(:user_survey) { Factory.create(:user_survey,)}

    it "should redirect to the survey" do
    end 

    it "should allow the user to submit the survey" do
    end

    it "should have a method to know if the user has completed the survey" do
    end

    it "should prevent the user from accessing the survey againi after completion" do
    end
  end

end
