require 'spec_helper'

describe Spree::UserSurveyQuestionOption do
  it "should belong to a survey question" do
    survey_question_option = FactoryGirl.build(:user_survey_question_option)
    survey_question_option.should respond_to(:user_survey_question) 
  end

  it "should have option name" do
    survey_question_option = FactoryGirl.build(:user_survey_question_option)
    survey_question_option.should respond_to(:option_name)
  end
end
