FactoryGirl.define do
  factory :survey_user, :parent => :user do
    user_survey { FactoryGirl.create(:user_survey) }
    survey_complete false
  end
end
