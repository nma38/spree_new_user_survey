FactoryGirl.define do
  factory :user_survey, :class => Spree::UserSurvey do
    #associations
    title { Faker::Lorem.sentence() }
  end
  
  factory :user_survey_question, :class => Spree::UserSurveyQuestion do
    #associations
    question { Faker::Lorem.paragraph() }
    user_survey { FactoryGirl.create(:user_survey) }
  end
  
  factory :user_survey_question_option, :class => Spree::UserSurveyQuestionOption do
    #associations
    option { Faker::Lorem.words(1) }
    user_survey_question { FactoryGirl.create(:user_survey_question) }
  end

  factory :user_survey_answer, :class => Spree::UserSurveyAnswer do
    #associations
    user { Factory.create(:survey_user) }
    user_survey_question { FactoryGirl.create(:user_survey_question) }
    question { user_survey_question.question  }
  end

  factory :user_survey_answer_option, :class => Spree::UserSurveyAnswerOption do
    #associations
    user_survey_answer { FactoryGirl.create(:user_survey_answer) }
  end
end
