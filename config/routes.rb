Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  namespace :admin do
    resources :user_surveys, :controller => "user_surveys" do
      resources :questions, :controller => "user_surveys/questions" do
        resources :options, :controller => "user_surveys/questions/options"
      end
    end
  end
  get 'survey', to: "user_survey#show"
end
