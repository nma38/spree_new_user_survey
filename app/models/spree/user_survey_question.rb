module Spree
  class UserSurveyQuestion < ActiveRecord::Base
    attr_accessible :user_survey, :question, :options
    belongs_to :user_survey, :class_name => "Spree::UserSurvey"
    belongs_to :question_type, :class_name => "Spree::UserSurveyQuestionType"
    has_many :options, :class_name => "Spree::UserSurveyQuestionOption", :dependent => :destroy
    has_many :answers, :class_name => "Spree::UserSurveyQuestionAnswer"    
  end
end
