if Spree.user_class
  Spree.user_class.class_eval do
    attr_accessible :survey_complete, :user_survey_id

    belongs_to :user_survey, :class_name => "Spree::UserSurvey"    
    has_many :user_survey_answer, :class_name => "Spree::UserSurveyAnswer", :dependent => :destroy

    def survey_complete?
      :survey_complete
    end

  end
end
