module Spree
  class UserSurveyAnswer < ActiveRecord::Base
    attr_accessible :user_survey_question, :options, :question

    validates :user, :presence => true
    if Spree.user_class
      belongs_to :user, :class_name => Spree.user_class.to_s
    else
      belongs_to :user
    end
    belongs_to :user_survey_question, :class_name => "Spree::UserSurveyQuestion"

    has_many :options, :class_name => "Spree::UserSurveyAnswerOption", :dependent => :destroy

    accepts_nested_attributes_for :options

    def option_attributes=(option_attributes)
      # Process the attributes hash
      option_attributes.each do |attributes|
        options.build(attributes)
      end
    end 
  end
end

