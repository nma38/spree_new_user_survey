module Spree
  class UserSurveyQuestionOption < ActiveRecord::Base
    attr_accessible :user_survey_question, :option
    belongs_to :user_survey_question, :class_name => "Spree::UserSurveyQuestion"
  end
end
