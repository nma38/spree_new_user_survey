module Spree
  class UserSurvey < ActiveRecord::Base
    attr_accessible :title
    
    #validates :user, :presence => true
    if Spree.user_class
      has_many :user, :class_name => Spree.user_class.to_s
    else
      has_many :user
    end

    has_many :user_survey_question, :class_name => "Spree::UserSurveyQuestion"
  end
end
