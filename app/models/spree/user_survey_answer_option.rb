module Spree
  class UserSurveyAnswerOption < ActiveRecord::Base
    attr_accessible :user_survey_answer, :checked, :answer
    belongs_to :user_survey_answer, :class_name => "Spree::UserSurveyAnswer"
  end
end

