module Spree
  module Admin
    class UserSurveysController < ResourceController
      before_filter :load_questions, :only => [:show, :edit]

      def show
        redirect_to( :action => :edit )
      end

      def index
        params[:q] ||= {}
        @search = UserSurvey.search(params[:q])
        @user_survey = @search.result.page(params[:page]).per(15)
      end

      def create
        create_or_update t("user_survey_successfully_created")
      end

      def update
        create_or_update t("user_survey_successfully_updated")
      end

      protected

      def load_questions
        @questions = UserSurveyQuestion.where(:user_survey_id => @user_survey.id)
      end
      
      private

      def create_or_update(flash_msg)
        if @user_survey.update_attributes(params[:user_survey])
          redirect_to edit_admin_user_survey_path(@user_survey)
          flash.notice = flash_msg
        else
          respond_with(@user_survey)
        end
      end

    end
  end
end

