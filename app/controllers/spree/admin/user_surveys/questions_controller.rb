module Spree
  module Admin
    module UserSurveys
      class QuestionsController < Spree::Admin::BaseController
        before_filter :load_user_survey
        before_filter :load_user_survey_question, :only => [:show, :edit, :update, :ship]
        before_filter :load_user_survey_question_options, :only => [:edit]
        
        def show
          redirect_to( :action => :edit )
        end
        
        def index
          @questions = UserSurveyQuestion.where(:user_survey_id => @survey.id)
        end
        
        def new
          @question = @survey.user_survey_question.build 
        end

        def create
          if (new_question = @survey.user_survey_question.create(params[:user_survey_question]))
            flash[:notice] = t('question_created')
            redirect_to admin_user_survey_question_path(@survey, new_question)
          else
            flash[:error] = t(:question_not_created)
            render :new
          end
        end

        def update
          if @question.update_attributes(params[:user_survey_question])
            flash[:notice] = t('question_updated')
            redirect_to admin_magazine_issue_path(@survey, @question)
          else
            flash[:error] = t(:question_not_updated)
            render :action => :edit
          end 
        end

        private

        def load_user_survey
          @survey = UserSurvey.find_by_id(params[:user_survey_id])
        end

        def load_user_survey_question
          @question = UserSurveyQuestion.find(params[:id])
        end
        
        def load_user_survey_question_options
          @question_options = UserSurveyQuestionOption.where(:user_survey_question_id => @question.id)
        end
      end
    end
  end
end
