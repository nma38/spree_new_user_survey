module Spree
  module Admin
    module UserSurveys
      module Questions
        class OptionsController < Spree::Admin::BaseController
         
          def show
            redirect_to( :action => 'edit' )
          end
          
        end
      end
    end
  end
end
