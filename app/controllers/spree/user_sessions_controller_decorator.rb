Spree::UserSessionsController.class_eval do
  
  private
  
  def after_sign_in_path_for(resource_or_scope)
    
    user = spree_current_user
    if user != nil and !user.survey_complete?
      redirect survey_path
    else
      stored_location_for(resource_or_scope) || signed_in_root_path(resource_or_scope)
    end
  end
end
