module Spree
  class UserSurveyController < Spree::StoreController
    before_filter :load_user
    before_filter :check_user_survey_exist
    
    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    respond_to :html

    def show
      survey = UserSurvey.find(@user.user_survey_id)
      if survey == nil
        redirect_to home_path
      end
      questions = UserSurveyQuestion.where(:user_survey_id => survey.id)
      @answers = Array.new
      questions.each do |question| 
        answer = UserSurveyAnswer.new(user_survey_question: question, question: question.question)
        
        #options = Array.new
        question.options.each do |question_option|
          answer.options.build(user_survey_answer: answer, answer:question_option.option)
          #UserSurveyAnswerOption.build
          #(user_survey_answer: answer, answer:question_option.option)
          #options.push(option)
        end

        #answer.options = options
        @answers.push(answer)
      end
    end

    def create
    
    end

    private

    def load_user
      @user = spree_current_user
      if @user == nil 
        redirect_to root_path
      end
    end
    
    def check_user_survey_exist
      if @user.survey_complete? == true
        redirect_to root_path  
      end
    end

    def create_user_answer(question)
      
    end
  end
end
