Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "user_surveys_admin_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:user_surveys) %>")
