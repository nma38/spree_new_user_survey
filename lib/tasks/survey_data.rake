require 'ffaker'
require 'pathname'
require 'spree/survey_data'

namespace :spree_survey_data do
  desc 'Loads survey data'
  task :load => :environment do
    if ARGV.include?("db:migrate")
      puts %Q{
Please run db:migrate separately from spree_survey_data:load.

Running db:migrate and spree_sample:load at the same time has been known to
cause problems where columns may be not available during sample data loading.

Migrations have been run. Please run "rake spree_survey_data:load" by itself now.
      }
      exit(1)
    end

    SpreeNewUserSurvey::Engine.load_survey
  end
end
