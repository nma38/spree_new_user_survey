module Spree
  class SurveyData 
    def self.load(file)
      path = File.expand_path(surveys_path + "#{file}.rb")
      # Check to see if the specified file has been loaded before
      if !$LOADED_FEATURES.include?(path)
        require path
        puts "Loaded #{file.titleize} samples"
      end
    end

    private

    def self.surveys_path
      Pathname.new(File.join(File.dirname(__FILE__), '..', '..', 'db', 'surveys'))
    end
  end
end
