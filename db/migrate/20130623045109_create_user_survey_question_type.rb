class CreateUserSurveyQuestionType < ActiveRecord::Migration
  def change
    create_table :spree_user_survey_question_types do |t|
      t.string :type_name
      t.timestamps
    end
  end
end
