class CreateUserSurveyQuestion < ActiveRecord::Migration
  def change
    create_table :spree_user_survey_questions do |t|
      t.string :question
      t.references :user_survey_question_type
      t.references :user_survey
      t.timestamps
    end
    add_index :spree_user_survey_questions, :user_survey_question_type_id, :name => 'type_index'
    add_index :spree_user_survey_questions, :user_survey_id

    create_table :spree_user_survey_question_options do |t|
      t.string :option
      t.references :user_survey_question
      t.timestamps
    end
    add_index :spree_user_survey_question_options, :user_survey_question_id, :name => 'question_index'
  end
end
