class CreateUserSurvey < ActiveRecord::Migration
  def self.up
    create_table :spree_user_surveys do |t|
      t.string :title
      t.timestamps
    end
  end

  def self.down
    drop_table :spree_user_surveys
  end
end
