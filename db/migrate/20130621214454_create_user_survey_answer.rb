class CreateUserSurveyAnswer < ActiveRecord::Migration
  def change
    create_table :spree_user_survey_answers do |t|
      t.references :user_survey_question
      t.references :user 
      t.string :question

      t.timestamps
    end
   # add_index :spree_user_survey_answers, :user_id
   # add_index :spree_user_survey_answers, :user_survey_question_id, :name => 'answer_question_index'

    create_table :spree_user_survey_answer_options do |t|
      t.references :user_survey_answer
      t.string :answer
      t.boolean :checked, :default => false

      t.timestamps
    end
    #add_index :spree_user_survey_answer_options, :user_survey_answer_id, :name => 'answer_index'
  end
end
