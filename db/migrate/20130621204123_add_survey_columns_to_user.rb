class AddSurveyColumnsToUser < ActiveRecord::Migration
  def change
    add_column :spree_users, :user_survey_id, :integer, :default => 1
    add_column :spree_users, :survey_complete, :boolean, :default => false
  end
end
