question_types = [
  {
    :type_name => "multiple"
  },
  {
    :type_name => "single"
  },
]

question_types.each do |type_attrs|
    Spree::UserSurveyQuestionType.create!(type_attrs, :without_protection => true)
end
