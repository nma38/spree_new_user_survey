Spree::SurveyData.load("question_type")

survey_info = {
  :title => "Sign up quiz"
}

Spree::UserSurvey.create!(survey_info, :without_protection => true)

survey = Spree::UserSurvey.find_by_title("Sign up quiz")
multiple_type = Spree::UserSurveyQuestionType.find_by_type_name("multiple")

survey_questions_info = [
  {
    :user_survey => survey,
    :question => "What is your favorite brand?",
    :question_type => multiple_type
  },
  {
    :user_survey => survey,
    :question => "What is your skin type?",
    :question_type => multiple_type
  },
]

survey_questions_info.each do |question_attrs|
  Spree::UserSurveyQuestion.create!(question_attrs, :without_protection => true)
end

brand_question = Spree::UserSurveyQuestion.find_by_question("What is your favorite brand?")
skin_question = Spree::UserSurveyQuestion.find_by_question("What is your skin type?")

survey_question_options_info = [
  {
    :user_survey_question => brand_question,
    :option => "clinique",
  },
  {
    :user_survey_question => brand_question,
    :option => "body shop",
  },
  {
    :user_survey_question => brand_question,
    :option => "estee laurier",
  },
]

survey_question_options_info.each do |option_attrs|
  Spree::UserSurveyQuestionOption.create!(option_attrs, :without_protection => true)
end


